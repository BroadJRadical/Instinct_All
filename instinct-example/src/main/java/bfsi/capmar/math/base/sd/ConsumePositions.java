/*
 * Copyright [2016] [Rajdeep Singh]
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package bfsi.capmar.math.base.sd;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicReference;

import org.apache.commons.math3.stat.descriptive.SynchronizedDescriptiveStatistics;

import bfsi.capmar.math.base.sd.PublishPositions.Position;
import in.net.broadjradical.instinct.annotation.Subscribe;
import in.net.broadjradical.instinct.annotation.Subscribe.ComMode;

public class ConsumePositions {
    public final Map<String, AtomicReference<Position>> max = new HashMap<>();
    public final Map<String, AtomicReference<Position>> maxDiff = new HashMap<>();
    public final Map<String, AtomicReference<Position>> minDiff = new HashMap<>();
    public final Map<String, SynchronizedDescriptiveStatistics> mean = new HashMap<>();

    public SynchronizedDescriptiveStatistics dStat = new SynchronizedDescriptiveStatistics(60);

    public ConsumePositions() {
        new Thread(new Runnable() {
            private final ConsumePositions consumer = ConsumePositions.this;
            private final Scanner scanner = new Scanner(System.in);
            @Override
            public void run() {
                while(true) {
                    try{
                        printResult(scanner.nextLine().split(" "));
                    } catch (Throwable e) {
                        System.err.println(e.toString() + " at " + e.getStackTrace()[0]);
                        System.out.println("command format : mean/sd <aapl/amzn/csco/ebay/fb/googl/intc/msft>");
                    }
                }
            }

            private void printResult(String[] cmd) {
                if(cmd.length != 2) {
                    System.out.println("command format : mean/sd <aapl/amzn/csco/ebay/fb/googl/intc/msft>");
                    return;
                }
                switch(cmd[0]) {
                    case "mean" :
                        if(consumer.mean.get(cmd[1]) == null)
                            System.out.println("data not available for company : " + cmd[1]);
                        else
                            print(cmd[0], cmd[1], consumer.mean.get(cmd[1]).getMean());
                        break;
                    case "sd" :
                        if(consumer.mean.get(cmd[1]) == null)
                            System.out.println("data not available for company : " + cmd[1]);
                        else
                            print(cmd[0], cmd[1], consumer.mean.get(cmd[1]).getStandardDeviation());
                        break;
                    default:
                        System.out.println("command not supported : " + cmd[0]);
                }
            }

            private void print(String cmd, String comp, Object result) {
                System.out.println("[command ::: " + cmd + ", company : " + comp + ", result ::: " + result + "]");
            }
        }).start();
    }

    @Subscribe(id = "all", communicationModel = ComMode.PUB_SUB, order = 0, active=false)
    public Position handlePosition(Position position) {
        if (!max.containsKey(position.comp)) {
            max.put(position.comp, new AtomicReference<Position>(null));
        }
        AtomicReference<Position> pos = max.get(position.comp);
        Position compPos = pos.get();
        long cnt = compPos == null ? -1 : Double.doubleToLongBits(compPos.data.getHigh().doubleValue());
        while (cnt < Double.doubleToLongBits(position.data.getHigh().doubleValue())
                && !pos.compareAndSet(compPos, position))
            ;
        return position;
    }

    @Subscribe(id = "all", communicationModel = ComMode.PUB_SUB, order = 0, active=false)
    public Position checkmaxDeviationPos(Position position) {
        if (!maxDiff.containsKey(position.comp)) {
            maxDiff.put(position.comp, new AtomicReference<Position>(null));
        }
        AtomicReference<Position> gl = maxDiff.get(position.comp);
        Position comPos = gl.get();
        while ((comPos == null || (Double.doubleToLongBits(
                comPos.data.getHigh().doubleValue() - comPos.data.getLow().doubleValue())) < (Double
                        .doubleToLongBits(
                                position.data.getHigh().doubleValue() - position.data.getLow().doubleValue())))
                && gl.compareAndSet(comPos, position));
        return position;
    }

    @Subscribe(id = "all", communicationModel = ComMode.PUB_SUB, order = 0, active=false)
    public Position checkMinDeviationPos(Position position) {
        if (!minDiff.containsKey(position.comp)) {
            minDiff.put(position.comp, new AtomicReference<Position>(null));
        }
        AtomicReference<Position> gl = minDiff.get(position.comp);
        Position comPos = gl.get();
        while ((comPos == null || (Double.doubleToLongBits(
                comPos.data.getHigh().doubleValue() - comPos.data.getLow().doubleValue())) > (Double
                        .doubleToLongBits(
                                position.data.getHigh().doubleValue() - position.data.getLow().doubleValue())))
                && gl.compareAndSet(comPos, position))
            ;
        return position;
    }

    @Subscribe(id="all", communicationModel=ComMode.PUB_SUB, order=0)
    public void calculateRunningMean(Position position) {
        if (!mean.containsKey(position.comp)) {
            mean.put(position.comp, new SynchronizedDescriptiveStatistics(60));
        }
        mean.get(position.comp).addValue(position.data.getHigh().add(position.data.getLow()).divide(BigDecimal.valueOf(2)).doubleValue());
    }
}
