/*
 * Copyright [2016] [Rajdeep Singh]
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package bfsi.capmar.math.base.sd.instinct;

import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicReference;

import bfsi.capmar.math.base.sd.ConsumePositions;
import bfsi.capmar.math.base.sd.PublishPositions;
import bfsi.capmar.math.base.sd.PublishPositions.Position;
import in.net.broadjradical.instinct.Instinct;

public class InstinctImpl {
    Instinct in;
    private static InstinctImpl instance;
    private static Object lock = new Object();

    private InstinctImpl() {

    }

    private void init() {
        in = new Instinct("bfsi.capmar.math.base.sd");
    }

    public static InstinctImpl getInstance() {
        if (null == instance) {
            synchronized (lock) {
                if (null == instance) {
                    instance = new InstinctImpl();
                    instance.init();
                }
            }
        }

        return instance;
    }

    public static void main(String[] args) throws Exception {
        InstinctImpl.getInstance().start();
    }

    private void start() throws Exception {
        PublishPositions positions = in.getBeanInstance(PublishPositions.class, true);
        positions.readData();
        ConsumePositions con = in.getBeanInstance(ConsumePositions.class, true);
        Thread.sleep(2000);
        for(Entry<String, AtomicReference<Position>> l : con.max.entrySet()) {
            System.out.print(" [" + l.getKey() + " : " + l.getValue().get() + "] ");
        }
        for(Entry<String, AtomicReference<Position>> l : con.maxDiff.entrySet()) {
            System.out.print(" [" + l.getKey() + " : " + l.getValue().get() + "] ");
        }
        for(Entry<String, AtomicReference<Position>> l : con.minDiff.entrySet()) {
            System.out.print(" [" + l.getKey() + " : " + l.getValue().get() + "] ");
        }
        System.out.println("command format : mean/sd <aapl/amzn/csco/ebay/fb/googl/intc/msft>");
    }

    public static class Mainfactory {
        public static final InstinctImpl getInstance() {
            return InstinctImpl.getInstance();
        }
    }
}
