/*
 * Copyright [2016] [Rajdeep Singh]
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package bfsi.capmar.math.base.sd.util;

public class MathUtil {
    public static final IDeviations getDeviationsCalculator() {return new OnlineDeviationAlgo();}

    public static interface IDeviations {
        double mean();
        double mean(double x);
        double remove(double x);
        double variance();
        double variance(double x);
        double standardDeviation();
        double standardDeviation(double x);
    }

    private static final class OnlineDeviationAlgo implements IDeviations {
        private long n = 0L;
        private double mean = 0.0;
        private double meanSq = 0.0;

        /* (non-Javadoc)
         * @see bfsi.capmar.math.base.sd.IDeviations#mean()
         */
        @Override
        public double mean() {
            return mean;
        }

        /* (non-Javadoc)
         * @see bfsi.capmar.math.base.sd.IDeviations#mean(double)
         */
        @Override
        public double mean(double x) {
            ++n;
            double nextM = mean + (x - mean) / n;
            meanSq += (x - mean) * (x - nextM);
            return mean = nextM;
        }

        /* (non-Javadoc)
         * @see bfsi.capmar.math.base.sd.IDeviations#remove(double)
         */
        @Override
        public double remove(double x) {
            if (n == 0L) {
                throw new IllegalStateException("counter set to 0");
            }
            if (n == 1L) {
                n = 0L;
                mean = 0.0;
                meanSq = 0.0;
                return mean;
            }
            double mOld = (n * mean - x)/(n - 1L);
            meanSq -= (x - mean) * (x - mOld);
            mean = mOld;
            --n;
            return mean;
        }

        /* (non-Javadoc)
         * @see bfsi.capmar.math.base.sd.IDeviations#variance()
         */
        @Override
        public double variance() {
            return n > 1 ? meanSq / n : 0.0;
        }

        /* (non-Javadoc)
         * @see bfsi.capmar.math.base.sd.IDeviations#variance(double)
         */
        @Override
        public double variance(double x) {
            mean(x);
            return n > 1 ? meanSq / n : 0.0;
        }

        /* (non-Javadoc)
         * @see bfsi.capmar.math.base.sd.IDeviations#standardDeviation()
         */
        @Override
        public double standardDeviation() {
            return Math.sqrt(variance());
        }

        /* (non-Javadoc)
         * @see bfsi.capmar.math.base.sd.IDeviations#standardDeviation(double)
         */
        @Override
        public double standardDeviation(double x) {
            return Math.sqrt(variance(x));
        }
    }
}
