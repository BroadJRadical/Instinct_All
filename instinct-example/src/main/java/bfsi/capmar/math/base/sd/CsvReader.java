/*
 * Copyright [2016] [Rajdeep Singh]
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package bfsi.capmar.math.base.sd;

import java.io.File;
import java.io.FileInputStream;
import java.math.BigDecimal;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

public class CsvReader {

    public static void main(String[] args) throws Exception {
        read(CsvReader.class.getResource("/aapl.csv").getPath());
        read(CsvReader.class.getResource("/amzn.csv").getPath());
        read(CsvReader.class.getResource("/csco.csv").getPath());
        read(CsvReader.class.getResource("/ebay.csv").getPath());
        read(CsvReader.class.getResource("/fb.csv").getPath());
        read(CsvReader.class.getResource("/googl.csv").getPath());
        read(CsvReader.class.getResource("/intc.csv").getPath());
        read(CsvReader.class.getResource("/msft.csv").getPath());
    }

    public static List<DataHolder> read(String file) throws Exception {
        File f = null;
        byte[] data = null;
        ByteBuffer dataBuff = null;
        byte[] dataSeg = null;
        DataHolder holder = new DataHolder();

        byte b = -1;
        final List<DataHolder> dataList = new ArrayList<CsvReader.DataHolder>(5000);
        Int ctr = new Int();
        Int lastPosition = new Int();
        Int currPosition = new Int();

        try (FileInputStream fStrm = new FileInputStream(f = new File(file))) {
            fStrm.read(data = new byte[(int) f.length()]);
        }
        dataBuff = ByteBuffer.allocateDirect(data.length);
        dataBuff.put(data);
        dataBuff.rewind();

        while(b < 0) {
            b = dataBuff.get();
        }
        lastPosition.setI(dataBuff.position());
        holder = new DataHolder();
        while (dataBuff.hasRemaining()) {
            newLine : while (true) {
                while (b != (byte) ',') {
                    b = dataBuff.get();
                    if(b == (byte) '\n') {
                        run(holder, dataBuff, dataSeg, ctr, lastPosition, currPosition);
                        break newLine;
                    }
                }
                run(holder, dataBuff, dataSeg, ctr, lastPosition, currPosition);
                b = dataBuff.get();
            }
            dataList.add(holder);
            holder = new DataHolder();
            ctr.setI(0);
            b = dataBuff.get();
        }

        return dataList;
    }

    private static void run(DataHolder holder, ByteBuffer dataBuff, byte[] dataSeg, Int ctr, Int lastPosition, Int currPosition) {
        currPosition.setI(dataBuff.position());
        dataBuff.position(lastPosition.getI() - 1);
        dataBuff.get(dataSeg = new byte[currPosition.getI() - (lastPosition.getI())], 0,
                currPosition.getI() - lastPosition.getI());
        if(dataBuff.limit() > currPosition.getI() + dataSeg.length)
            dataBuff.position(lastPosition.setI(currPosition.getI() + 1));
        else
            return;
        holder.setData(ctr.setI(ctr.getI() + 1) - 1, dataSeg);
    }

    public static class DataHolder {
        private byte[][] data = new byte[6][];

        public void setData(int i, byte[] val) {
            data[i] = val;
        }

        public byte[][] getData() {
            return data;
        }

        public String getDate() {
            return new String(data[0]);
        }

        public BigDecimal getOpen() {
            return BigDecimal.valueOf(Double.valueOf(new String(data[1])));
        }

        public BigDecimal getHigh() {
            return BigDecimal.valueOf(Double.valueOf(new String(data[2])));
        }

        public BigDecimal getLow() {
            return BigDecimal.valueOf(Double.valueOf(new String(data[3])));
        }

        public BigDecimal getClose() {
            return BigDecimal.valueOf(Double.valueOf(new String(data[4])));
        }

        public String getVolume() {
            if(data[5] != null)
                return new String(data[5]);
            else return "null";
        }

        @Override
        public String toString() {
            return "DataHolder [" + (getDate() != null ? "getDate()=" + getDate() + ", " : "")
                    + (getOpen() != null ? "getOpen()=" + getOpen() + ", " : "")
                    + (getHigh() != null ? "getHigh()=" + getHigh() + ", " : "")
                    + (getLow() != null ? "getLow()=" + getLow() + ", " : "")
                    + (getClose() != null ? "getClose()=" + getClose() + ", " : "")
                    + (getVolume() != null ? "getVolume()=" + getVolume() : "") + "]";
        }
    }

    private static class Int {
        private int i;
        int setI(int i) {
            return this.i = i;
        }
        int getI() {
            return i;
        }
        @Override
        public String toString() {
            return "Int [i=" + i + "]";
        }
    }
}
