/*
 * Copyright [2016] [Rajdeep Singh]
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package bfsi.capmar.math.base.sd;

import java.util.List;
import java.util.concurrent.locks.LockSupport;

import bfsi.capmar.math.base.sd.CsvReader.DataHolder;
import bfsi.capmar.math.base.sd.instinct.InstinctImpl;
import bfsi.capmar.math.base.sd.instinct.InstinctImpl.Mainfactory;
import in.net.broadjradical.instinct.annotation.BeanFactory;
import in.net.broadjradical.instinct.annotation.BeanFactory.FactoryMethod;
import in.net.broadjradical.instinct.annotation.Publish;
import in.net.broadjradical.instinct.common.InstinctThreadBehavior;
import in.net.broadjradical.instinct.common.InstinctThreadBehavior.InstinctWaitBehavior;

public class PublishPositions {
    private ConsumePositions consumer;

    @BeanFactory(type = Mainfactory.class, hasStaticMethods=true, factoryMethods={
            @FactoryMethod(factoryMethod="getInstance", paramName="inst")
    })
    public PublishPositions(InstinctImpl inst) {
//        this.consumer = inst.in.getBeanInstance(ConsumePositions.class, true);
    }

    public void readData() throws Exception {
        startThread(new Runnable() {@Override
        public void run() {try{publishFromList("aapl", CsvReader.read(CsvReader.class.getResource("/instruments/aapl.csv").getPath()));}catch(Exception e){}}});
        startThread(new Runnable() {@Override
        public void run() {try{publishFromList("amzn", CsvReader.read(CsvReader.class.getResource("/instruments/amzn.csv").getPath()));}catch(Exception e){}}});
        startThread(new Runnable() {@Override
        public void run() {try{publishFromList("csco", CsvReader.read(CsvReader.class.getResource("/instruments/csco.csv").getPath()));}catch(Exception e){}}});
        startThread(new Runnable() {@Override
        public void run() {try{publishFromList("ebay", CsvReader.read(CsvReader.class.getResource("/instruments/ebay.csv").getPath()));}catch(Exception e){}}});
        startThread(new Runnable() {@Override
        public void run() {try{publishFromList("fb", CsvReader.read(CsvReader.class.getResource("/instruments/fb.csv").getPath()));}catch(Exception e){}}});
        startThread(new Runnable() {@Override
        public void run() {try{publishFromList("googl", CsvReader.read(CsvReader.class.getResource("/instruments/googl.csv").getPath()));}catch(Exception e){}}});
        startThread(new Runnable() {@Override
        public void run() {try{publishFromList("intc", CsvReader.read(CsvReader.class.getResource("/instruments/intc.csv").getPath()));}catch(Exception e){}}});
        startThread(new Runnable() {@Override
        public void run() {try{publishFromList("msft", CsvReader.read(CsvReader.class.getResource("/instruments/msft.csv").getPath()));}catch(Exception e){}}});
    }

    private void startThread(Runnable r) {
        new Thread(r).start();
    }

    public int publishFromList(final String comp, final List<DataHolder> list) {
        long start = System.currentTimeMillis();
        int in = 0;
        for (DataHolder data : list) {
            if(in++ == 0){
                continue;
            }
            LockSupport.parkNanos(50000000);
            publishAll(comp, data);
            switch (comp) {
                case "appl":
                    publishAppl(data);
                case "amzn":
                    publishAmazon(data);
                case "csco":
                    publishCisco(data);
                case "ebay":
                    publishEbay(data);
                case "fb":
                    publishFaceBook(data);
                case "googl":
                    publishGoogle(data);
                case "intc":
                    publishIntel(data);
                case "msft":
                    publishMsft(data);
            }
        }
        System.out.println("time taken to publish trades for : " + comp + " : " + (System.currentTimeMillis() - start) + "ms");
        return list.size();
    }

    @Publish(id = "apple", behavior = InstinctWaitBehavior.BUSY_WAIT, threadBehavior = InstinctThreadBehavior.MPCS)
    public Position publishAppl(DataHolder data) {
        return new Position("Apple", data);
    }

    @Publish(id = "amazon", behavior = InstinctWaitBehavior.BUSY_WAIT, threadBehavior = InstinctThreadBehavior.MPCS)
    public Position publishAmazon(DataHolder data) {
        return new Position("Amazon", data);
    }

    @Publish(id = "cisco", behavior = InstinctWaitBehavior.BUSY_WAIT, threadBehavior = InstinctThreadBehavior.MPCS)
    public Position publishCisco(DataHolder data) {
        return new Position("Cisco", data);
    }

    @Publish(id = "ebay", behavior = InstinctWaitBehavior.BUSY_WAIT, threadBehavior = InstinctThreadBehavior.MPCS)
    public Position publishEbay(DataHolder data) {
        return new Position("Ebay", data);
    }

    @Publish(id = "facebook", behavior = InstinctWaitBehavior.BUSY_WAIT, threadBehavior = InstinctThreadBehavior.MPCS)
    public Position publishFaceBook(DataHolder data) {
        return new Position("Facebook", data);
    }

    @Publish(id = "google", behavior = InstinctWaitBehavior.BUSY_WAIT, threadBehavior = InstinctThreadBehavior.MPCS)
    public Position publishGoogle(DataHolder data) {
        return new Position("Google", data);
    }

    @Publish(id = "intel", behavior = InstinctWaitBehavior.BUSY_WAIT, threadBehavior = InstinctThreadBehavior.MPCS)
    public Position publishIntel(DataHolder data) {
        return new Position("Intel", data);
    }

    @Publish(id = "msft", behavior = InstinctWaitBehavior.BUSY_WAIT, threadBehavior = InstinctThreadBehavior.MPCS)
    public Position publishMsft(DataHolder data) {
        return new Position("MSFT", data);
    }

    @Publish(id = "all", behavior = InstinctWaitBehavior.BUSY_WAIT, threadBehavior = InstinctThreadBehavior.SPMC)
    public Position publishAll(String comp, DataHolder data) {
        return new Position(comp, data);
    }

    public static final class Position {
        public final String comp;
        public final DataHolder data;

        public Position(String comp, DataHolder holder) {
            this.comp = comp;
            this.data = holder;
        }

        @Override
        public String toString() {
            return "Position [" + (comp != null ? "comp=" + comp + ", " : "") + (data != null ? "data=" + data : "")
                    + "]";
        }
    }
}
